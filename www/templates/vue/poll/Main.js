import Vue from 'vue';

import VotingApp from './VotingApp.vue';
import {EventBus} from '../EventBus';

window.initVotingApp = function(reactiveData) {
    const globals = new Vue({
        data: reactiveData,
    });
    globals.install = function () {
        Object.defineProperty(Vue.prototype, '$globals', {
            get() {
                return globals;
            },
            configurable: true,
        });
    };
    Vue.use(globals);

    let app = new Vue({
        el: '#voting-app',
        render: h => h(VotingApp),
    });
    return {
        app: app,
        eventBus: EventBus,
    };
};
