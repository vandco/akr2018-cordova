import BaseController from './BaseController';
import Schedule from './schedule';
import LectureController from './LectureController';

var _speakerController = Object.create(BaseController);
Object.defineProperty(_speakerController, '_pageName', {value: 'programme/speaker'});
Object.defineProperty(_speakerController, '_pageTitle', {value: 'Спикер'});
Object.defineProperty(_speakerController, '_speaker', {writable: true});
Object.defineProperty(_speakerController, '_speakerRoles_cached', {writable: true, value: {}});
Object.defineProperty(_speakerController, 'onInit', {value: function ($pageContainer, page) {
    this._speaker = Schedule.speakers[page.query.speakerID];

    var $content = $pageContainer.find('.content-block').html('');
    var $preloader = $$('<span />');
    $preloader.addClass('preloader').css('width', '50px').css('height', '50px').html($$('#tpl-preloader').html());
    $preloader.appendTo($content);

    setTimeout((function () {
        var $collection = this._buildDOM();
        $collection.appendTo($content);
        $preloader.remove();
    }).bind(this), 100);

    this.on('click', '.card', (function (e) {
        console.log('this:');
        console.log(this);
        console.log(e);

        var $card = $$(e.target);
        while (!$card.is('.card')) {
            $card = $card.parent();
        }

        console.log('Card clicked:');
        console.log($card);

        this.onCardClick($card);
    }).bind(this));
}});
Object.defineProperty(_speakerController, 'onAfterAnimation', {value: function (page) {
    var $navbarRight = window.app.$getNavbar($$('#' + this._pageID)).find('.right');
    var $btnBack = $$('<a href="#" class="link back" />');
    $btnBack.html(_('back')).appendTo($navbarRight);
    window.myApp.sizeNavbars('.view-main');

    this.onBackButton = function() {
        $btnBack.click();
        return false;
    };
}});
Object.defineProperty(_speakerController, 'onCardClick', {
    value: (function ($card) {
        switch ($card.attr('data-role')) {
            case 'lecture':
                LectureController().load({
                    lectureID: parseInt($card.attr('data-lecture-id'))
                });
                break;
            default:
                window.myApp.alert('Непонятная карточка!');
                break;
        }
    }).bind(_speakerController)
});

Object.defineProperty(_speakerController, '_buildDOM', {value: function() {
    var $collection = $$();

    // Speaker Photo
    var photoSrc = '';
    if (typeof this._speaker.photoName !== typeof undefined) {
        photoSrc = cordova.file.cacheDirectory + 'unpacked/' + this._speaker.photoName;
    } else {
        if (this._speaker.sex === 'female') {
            photoSrc = 'img/female_100.png';
        } else {
            photoSrc = 'img/male_100.png';
        }
    }

    var $img = $$('<div class="speaker-photo" />');
    $img
        .css({
            'background': `url('${photoSrc}') no-repeat center center / cover`,
            'margin': '10px auto',
            'display': 'block',
            'border-radius': '50%',
            'width': '100px',
            'height': '100px',
        });
    $collection.add($img);


    // Speaker Name
    var $h1 = $$('<h1 />');
    var speakerName;
    if (
        (typeof this._speaker['lastName_' + window.language] === typeof '')
        && (this._speaker['lastName_' + window.language].trim() !== '')
    ) {
        speakerName = this._speaker['lastName_' + window.language] + ' ' + this._speaker['firstName_' + window.language];
    } else {
        speakerName = this._speaker['firstName_' + window.language];
    }
    if (this._speaker['city']) {
        speakerName += ' (' + this._speaker['city'] + ')';
    }
    $h1.html(speakerName);
    $collection.add($h1);



    // Timetable
    var $timetable = $$('<div class="timetable" />');
    $collection.add($timetable);


    setTimeout((function () {
        this._rebuildTimetableDOM();
    }).bind(this), 0);


    return $collection;
}});

Object.defineProperty(_speakerController, '_rebuildTimetableDOM', {value: function () {
    var $content = $$('#' + this._pageID).find('.timetable').html('');
    var $preloader = $$('<span />');
    $preloader.addClass('preloader').css('width', '50px').css('height', '50px').html($$('#tpl-preloader').html());
    $preloader.appendTo($content);

    setTimeout((function () {
        var $collection;
        $collection = this._buildDOMByChronology();
        $collection.appendTo($content);
        $preloader.remove();
    }).bind(this), 100);
}});

Object.defineProperty(_speakerController, '_buildDOMByChronology', {value: function() {
    var $collection = $$();
    var $list = null;
    var $ul = null;
    var $card;

    var events = [];
    this._speaker.lectures.forEach(function (lecture) {
        if (lecture.sessions.length != 1) {
            return;
        }

        events.push(lecture);
    });

    var fieldName = 'title_' + window.language;
    events.sort(
        firstBy(function (a, b) {
            if (a.start.getTime() > b.start.getTime()) return 1;
            if (a.start.getTime() == b.start.getTime()) return 0;
            if (a.start.getTime() < b.start.getTime()) return -1;
        }).thenBy(function(a, b) {
            if (a.end.getTime() > b.end.getTime()) return 1;
            if (a.end.getTime() == b.end.getTime()) return 0;
            if (a.end.getTime() < b.end.getTime()) return -1;
        }).thenBy(function(a, b) {
            if (a[fieldName] >  b[fieldName]) return 1;
            if (a[fieldName] == b[fieldName]) return 0;
            if (a[fieldName] <  b[fieldName]) return -1;
        })
    );

    $list = $$('<div />');
    $list.addClass('list-block cards-list');
    $ul = $$('<ul />');
    events.forEach((function (event) {
        if (event.toString() === '[object Lecture]') {
            let context = {
                language: window.language,
                lecture: event,
                date: true,
                activeSpeaker: this._speaker
            };
            let compiledHtml = window.compiledTpls.lectureCard(context).trim();
            $card = $$(compiledHtml);

            $card.appendTo($ul);
        }
    }).bind(this));
    $ul.appendTo($list);
    $collection.add($list);


    return $collection;
}});

let SpeakerController = () => _speakerController._construct();
export default SpeakerController;
