var BaseController = Object.create(null);
Object.defineProperty(BaseController, '_controllersStack', {writable: true, value: []});
Object.defineProperty(BaseController, '_callbacks', {writable: true, value: {}});
Object.defineProperty(BaseController, '_pageName', {});
Object.defineProperty(BaseController, '_pageID', {});
Object.defineProperty(BaseController, '_pageTitle', {});
Object.defineProperty(BaseController, 'onInit', {value: function($pageContainer, page) {}});
Object.defineProperty(BaseController, 'onAfterAnimation', {value: function(page) {}});
Object.defineProperty(BaseController, 'onBack', {value: function(page) {}});
Object.defineProperty(BaseController, '_constructed', {value: false, writable: true});
Object.defineProperty(BaseController, '_construct', {value: function () {
    var thisPtr = this;

    if (!this._constructed) {
        Object.defineProperty(thisPtr, '_pageID', {writable: true});
        Object.defineProperty(thisPtr, '_leftPanelState', {writable: true});
        Object.defineProperty(thisPtr, 'onBackButton', {value: function() {
            return true;
        }, writable: true});
        Object.defineProperty(thisPtr, 'onMenuButton', {value: function() {
            return true;
        }, writable: true});

        if (this._pageName in BaseController._callbacks) {
            if ('pageInit' in BaseController._callbacks[this._pageName]) {
                BaseController._callbacks[this._pageName].pageInit.remove();
            }
            if ('pageAfterAnimation' in BaseController._callbacks[this._pageName]) {
                BaseController._callbacks[this._pageName].pageAfterAnimation.remove();
            }
            if ('pageBack' in BaseController._callbacks[this._pageName]) {
                BaseController._callbacks[this._pageName].pageBack.remove();
            }
        } else {
            BaseController._callbacks[this._pageName] = {};
        }

        document.addEventListener('pause', function() {
            var latestController = BaseController._controllersStack[BaseController._controllersStack.length - 1].controller;
            if (typeof latestController.onAppPause === typeof (function () {})) {
                latestController.onAppPause();
            }
        }, false);
        document.addEventListener('resume', function() {
            var latestController = BaseController._controllersStack[BaseController._controllersStack.length - 1].controller;
            if (typeof latestController.onAppResume === typeof (function () {})) {
                latestController.onAppResume();
            }
        }, false);

        BaseController._callbacks[this._pageName].pageInit = window.myApp.onPageInit(this._pageName, function (page) {
            var $pageContainer = $$(page.container);
            thisPtr._pageID = 'seq-' + (new Date()).getTime();
            BaseController._controllersStack.push({
                controller: thisPtr,
                id: thisPtr._pageID
            });
            $pageContainer.attr('id', thisPtr._pageID);

            thisPtr._leftPanelState = window.myApp.params.swipePanel;
            window.myApp.params.swipePanel = 'left';

            thisPtr.onInit($pageContainer, page);
        });

        BaseController._callbacks[this._pageName].pageAfterAnimation = window.myApp.onPageAfterAnimation(this._pageName, function (page) {
            var $pageContainer = $$(page.container);
            thisPtr._pageID = $pageContainer.attr('id');
            window.app.resetNavBar();
            if (thisPtr._pageTitle) {
                window.app.setTitle(thisPtr._pageTitle);
            } else if (window.isAndroid) {
                window.app.setTitle('АКР 2018');
            }
            thisPtr.onAfterAnimation(page);
        });

        BaseController._callbacks[this._pageName].pageBack = window.myApp.onPageBack(this._pageName, function (page) {
            thisPtr.onBack(page);

            window.myApp.params.swipePanel = thisPtr._leftPanelState;

            /*thisPtr._handlers.forEach(function (handlerObj) {
                thisPtr._off(handlerObj.eventName, handlerObj.selector, handlerObj.handler);
            });
            thisPtr._handlers.length = 0; // remove all (fully complies with ECMAScript 5)*/

            BaseController._controllersStack.pop();
        });

        Object.defineProperty(this, '_handlers', {writable: true, value: []});

        this._constructed = true;
    }
    return this;
}});
Object.defineProperty(BaseController, 'load', {value: function (query, ignoreCache) {
    if (typeof ignoreCache === typeof undefined) ignoreCache = false;

    console.log('MVC Load Page: ' + this._pageName + ', query:');
    console.log(query);

    var urlNoCacheSuffix = '';
    if (ignoreCache) urlNoCacheSuffix = '?' + Math.random();

    if (BaseController._controllersStack.length) {
        var latestController = BaseController._controllersStack[BaseController._controllersStack.length - 1].controller;
        if (typeof latestController.onUnload === typeof (function () {})) {
            var needToPop = latestController.onUnload();
            if (needToPop) {
                console.log('Controller is popping');
                BaseController._controllersStack.pop();
            }
        }
    }

    window.mainView.router.load({
        url: this._pageName + '.html' + urlNoCacheSuffix,
        query: query
    });
}});
Object.defineProperty(BaseController, 'on', {value: function (eventName, selector, handler) {
    $$(document).on(eventName, '#' + this._pageID + ' ' + selector, handler);
    this._handlers.push({
        eventName: eventName,
        selector: selector,
        handler: handler
    });
}});
Object.defineProperty(BaseController, 'off', {value: function (eventName, selector, handler) {
    this._off(eventName, selector, handler);

    var idxes = [];
    this._handlers.forEach(function (handlerObj, idx) {
        if (
            (handlerObj.eventName === eventName)
            && (handlerObj.selector === selector)
            && (handlerObj.handler === handler)
        ) {
            idxes.push(idx);
        }
    });
    idxes.forEach((function (idx) {
        this._handlers.splice(idx, 1);
    }).bind(this));
}});
Object.defineProperty(BaseController, '_off', {value: function (eventName, selector, handler) {
    $$(document).off(eventName, '#' + this._pageID + ' ' + selector, handler);
}});

export default BaseController;
