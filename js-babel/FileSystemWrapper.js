let fileSystemWrapper = {
    openFileAsync: (dirEntry, fileName, options) => new Promise((resolve, reject) => {
        dirEntry.getFile(fileName, options, resolve, reject);
    }),
    removeFileAsync: (dirEntry, fileName, skipNonExisting) => new Promise((resolve, reject) => {
        dirEntry.getFile(fileName, {
            create: false,
            exclusive: false,
        }, fileEntry => {
            console.log(`removing ${fileName}`);
            fileEntry.remove(resolve, reject);
        }, error => {
            try {
                console.log(`not found while removing ${fileName}`);
                if (skipNonExisting && (error.code === FileError.NOT_FOUND_ERR)) {
                    console.log("it's ok");
                    resolve();
                } else {
                    console.log("it's fail");
                    reject(error);
                }
            } catch (e) {
                reject(e);
            }
        });
    }),
};

let FileSystemWrapper = new Proxy(fileSystemWrapper, {});

export default FileSystemWrapper;
