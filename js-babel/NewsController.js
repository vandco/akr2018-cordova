import BaseController from './BaseController';
import LocalStorageWrapper from './LocalStorageWrapper';
import NewsSingleController from './NewsSingleController';

var _newsController = Object.create(BaseController);
Object.defineProperty(_newsController, '_pageName', {value: 'news'});
Object.defineProperty(_newsController, '_pageTitle', {value: 'Новости'});
//Object.defineProperty(_newsController, '_page', {writable: true});
Object.defineProperty(_newsController, 'onInit', {value: function ($pageContainer, page) {
    LocalStorageWrapper.setItemAsync('newsSeenIDs', JSON.stringify(Object.keys(window.news)));
    var news = [];
    for (var key in window.news) {
        news.push(window.news[key]);
    }
    news.sort(function(a, b) {
        if (a.published_at < b.published_at) return 1;
        if (a.published_at == b.published_at) return 0;
        if (a.published_at > b.published_at) return -1;
    });
    var context = {
        language: window.language,
        news: news
    };
    var compiledHtml = window.compiledTpls.news(context).trim();
    $pageContainer.find('.content-block').html(compiledHtml);
    $$('#left-menu-tpl').find('.news-unread-badge').remove();

    this.on('click', '.card', (function (e) {
        console.log('this:');
        console.log(this);
        console.log(e);

        var $card = $$(e.target);
        while (!$card.is('.card')) {
            $card = $card.parent();
        }

        console.log('Card clicked:');
        console.log($card);

        this.onCardClick($card);
    }).bind(this));
}});
Object.defineProperty(_newsController, 'onAfterAnimation', {value: function (page) {
    var $navbarRight = window.app.$getNavbar($$('#' + this._pageID)).find('.right');
    //var $btnBack = $$('<a href="#" class="link back" />');
    //$btnBack.html(_('back')).appendTo($navbarRight);
    window.myApp.sizeNavbars('.view-main');

    /*this.onBackButton = function() {
        $btnBack.click();
        return false;
    };*/
}});

Object.defineProperty(_newsController, 'onCardClick', {
    value: (function ($card) {
        NewsSingleController().load({
            newsID: parseInt($card.attr('data-news-id'))
        });
    }).bind(_newsController)
});



let NewsController = () => _newsController._construct();
export default NewsController;
