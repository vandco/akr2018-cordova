import BaseController from './BaseController';
import ProgrammeController from './ProgrammeController';
import Handlebars from '../node_modules/handlebars';
import LocalStorageWrapper from './LocalStorageWrapper';
import Schedule from './schedule';
import MyAgenda from './my-agenda';
import FileSystemWrapper from './FileSystemWrapper';

const oldConsoleLog = window.console.log;
const oldConsoleErr = window.console.error;
window.log = [];
window.console.log = function(msg) {
    oldConsoleLog.apply(window.console, arguments);
    window.log.push({type: 'log', msg: msg});
};
window.console.error = function(msg) {
    oldConsoleErr.apply(window.console, arguments);
    window.log.push({type: 'error', msg: msg});
};

window.controllers = {
    ProgrammeController,
    CustomPageController: require('./CustomPageController').default,
    LectureController: require('./LectureController').default,
    MapController: require('./MapController').default,
    MyAgendaController: require('./MyAgendaController').default,
    NewsController: require('./NewsController').default,
    NewsSingleController: require('./NewsSingleController').default,
    QuestionsController: require('./QuestionsController').default,
    SettingsController: require('./SettingsController').default,
    SpeakersController: require('./SpeakersController').default,
    SpeakerController: require('./SpeakerController').default,
    VotingController: require('./VotingController').default,
};

window.myApp = null;
window.mainView = null;
window.language = null;
window.style = 'ios';
window.isGod = null;
window.$$ = Dom7;
window.isAndroid = Framework7.prototype.device.android === true;
window.isIos = Framework7.prototype.device.ios === true;
window.lockedBackgroundVerification = false;
window.localTimezoneOffset = (new Date()).getTimezoneOffset();

if (window.isAndroid) {
    window.style = 'material';
}

window.app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
        let thisPtr = this;

        // Handle error reporting
        window.addEventListener('error', (e) => {
            console.error('error event listener');
            console.error(e);
            console.groupCollapsed();
            console.log(e.error.stack);
            console.groupEnd();

            const breakApp = () => {
                if (window.isAndroid) {
                    navigator.app.exitApp();
                } else {
                    window.myApp.modal({
                        text: 'Ошибка. Продолжение работы невозможно.',
                        buttons: [],
                    });
                }
            };

            window.myApp.confirm('Произошла ошибка. Подготовить отчёт разработчикам?', () => {
                const startGatheringDataTimestamp = (new Date()).getTime();

                window.plugins.spinnerDialog.show('Сбор данных', 'Подготовка отчёта...', true);

                let cache = []; // to avoid error while stringifying circular objects

                let log = JSON.stringify(window.log, (key, value) => {
                    if (typeof value === 'object' && value !== null) {
                        if (cache.indexOf(value) !== -1) {
                            // Duplicate reference found
                            try {
                                // If this value does not reference a parent it can be deduped
                                return JSON.parse(JSON.stringify(value));
                            } catch (e) {
                                // discard key if value cannot be deduped
                                return `*** DUPLICATE OF ${key} ***`;
                            }
                        }
                        // Store value in our collection
                        cache.push(value);
                    }
                    return value;
                });
                cache = [];

                let stack = JSON.stringify(e.error.stack, (key, value) => {
                    if (typeof value === 'object' && value !== null) {
                        if (cache.indexOf(value) !== -1) {
                            // Duplicate reference found
                            try {
                                // If this value does not reference a parent it can be deduped
                                return JSON.parse(JSON.stringify(value));
                            } catch (e) {
                                // discard key if value cannot be deduped
                                return `*** DUPLICATE OF ${key} ***`;
                            }
                        }
                        // Store value in our collection
                        cache.push(value);
                    }
                    return value;
                });
                cache = [];

                let fullErrObj = JSON.stringify(e, (key, value) => {
                    if (typeof value === 'object' && value !== null) {
                        if (cache.indexOf(value) !== -1) {
                            // Duplicate reference found
                            try {
                                // If this value does not reference a parent it can be deduped
                                return JSON.parse(JSON.stringify(value));
                            } catch (e) {
                                // discard key if value cannot be deduped
                                return `*** DUPLICATE OF ${key} ***`;
                            }
                        }
                        // Store value in our collection
                        cache.push(value);
                    }
                    return value;
                });
                cache = [];

                let ua = navigator.userAgent;

                LocalStorageWrapper.keysAsync()
                    .then((keys) => {
                        let promises = [];
                        for (let key of keys) {
                            promises.push(LocalStorageWrapper.getItemAsync(key, '*** MISSING ***'));
                        }
                        let nativeStorage = {};
                        return Promise.all(promises)
                            .then((values) => {
                                for (let [idx, key] of keys.entries()) {
                                    nativeStorage[key] = values[idx];
                                }
                                return nativeStorage;
                            });
                    })
                    .then((nativeStorage) => {
                        return cordova.getAppVersion.getVersionNumber()
                            .then((version) => ({nativeStorage, version}));
                    })
                    .then(({nativeStorage, version}) => {
                        const endGatheringDataTimestamp = (new Date()).getTime();
                        const waitMore = 1500 - (endGatheringDataTimestamp - startGatheringDataTimestamp);

                        const data = {
                            date: (new Date()).toString(),
                            localStorage: JSON.stringify(window.localStorage),
                            nativeStorage: JSON.stringify(nativeStorage),
                            fullErrorObj: fullErrObj,
                            message: e.message,
                            file: e.filename,
                            line: e.lineno,
                            column: e.colno,
                            log,
                            stack,
                            ua,
                            version
                        };

                        // https://stackoverflow.com/a/6566471/2175025
                        const encodeObjToURI = (obj) => {
                            let str = '';
                            for (let key in obj) {
                                if (str !== '') {
                                    str += '&';
                                }
                                str += key + '=' + encodeURIComponent(obj[key]);
                            }
                            return str;
                        };
                        const dataSerialized = encodeObjToURI(data);

                        // https://stackoverflow.com/a/14919494/2175025
                        const humanFileSize = (bytes, si) => {
                            const thresh = si ? 1000 : 1024;
                            if (Math.abs(bytes) < thresh) {
                                return bytes + ' B';
                            }
                            const units = si
                                ? ['kB','MB','GB','TB','PB','EB','ZB','YB']
                                : ['KiB','MiB','GiB','TiB','PiB','EiB','ZiB','YiB'];
                            let u = -1;
                            do {
                                bytes /= thresh;
                                ++u;
                            } while (Math.abs(bytes) >= thresh && u < units.length - 1);
                            return bytes.toFixed(1) + ' ' + units[u];
                        };

                        const size = humanFileSize(dataSerialized.length, true);

                        return new Promise((resolve, reject) => {
                            setTimeout(() => {
                                window.plugins.spinnerDialog.hide();
                                window.myApp.confirm(`Размер отчёта: <b>${size}</b>. <br /><br /> Отправить отчёт разработчикам?`, () => {
                                    resolve(data);
                                }, breakApp);
                            }, Math.max(waitMore, 0));
                        });
                    })
                    .then((data) => {
                        let sendReport;
                        sendReport = () => {
                            window.plugins.spinnerDialog.show('Отправка отчёта...', _('please wait'), true);

                            $$.ajax({
                                url: 'http://akr-congress2018.ru/mobile-app/error-report',
                                //crossDomain: true,
                                //dataType: 'json',
                                timeout: 5000,
                                method: 'post',
                                data: data,
                                success() {
                                    window.myApp.alert('Отчёт об ошибке принят. Мы постараемся устранить ошибку как можно скорее. Спасибо, что помогаете улучшить приложение!', breakApp);
                                },
                                error(xhr, status) {
                                    console.log(xhr);
                                    console.log(status);
                                    if (status === 'timeout') {
                                        window.myApp.confirm('Время ожидания ответа от сервера истекло. Повторить отправку?', sendReport, breakApp);
                                    } else {
                                        window.myApp.confirm('Ошибка связи. Пожалуйста, проверьте подключение к интернету. Повторить отправку?', sendReport, breakApp);
                                    }
                                },
                                complete() {
                                    window.plugins.spinnerDialog.hide();
                                },
                            });
                        };
                        sendReport();
                    })
                    .catch((e) => {
                        window.plugins.spinnerDialog.hide();
                        console.error('Error while error reporting! :-)');
                        console.error(e);
                        window.myApp.modal({
                            text: 'Ошибка. Продолжение работы невозможно.',
                            buttons: [],
                        });
                    });
            });
        });

        if (window.isAndroid) {
            let exitAppModalShown = false;
            document.addEventListener("backbutton", function (e) {
                e.preventDefault();
                console.log('Back Button pressed');
                if ($$('.panel-left').hasClass('active')) {
                    window.myApp.closePanel(true);
                } else {
                    if (
                        (typeof BaseController._controllersStack === typeof undefined)
                        || (!BaseController._controllersStack.length)
                        || (BaseController._controllersStack.slice(-1)[0].controller.onBackButton())
                    ) {
                        if (!exitAppModalShown) {
                            window.myApp.confirm(_('exit app text'), _('exit app title'), function () {
                                navigator.app.exitApp();
                            }, function () {
                                exitAppModalShown = false;
                            });
                            exitAppModalShown = true;
                        }
                    }
                }
            }, false);

            navigator.app.overrideButton("menubutton", true);  // Workaround Cordova bug
            document.addEventListener("menubutton", function (e) {
                e.preventDefault();
                console.log('Menu Button pressed');
                if (
                    (typeof BaseController._controllersStack === typeof undefined)
                    || (!BaseController._controllersStack.length)
                    || (BaseController._controllersStack.slice(-1)[0].controller.onMenuButton())
                ) {
                    if ($$('.panel-left').hasClass('active')) {
                        window.myApp.closePanel(true);
                    } else {
                        window.myApp.openPanel('left', true);
                    }
                }
            }, false);
        } else if (window.isIos) {
            if (typeof window.Keyboard !== typeof undefined) {
                window.Keyboard.shrinkView(true);
            }
        }

        // правильная работа с часовыми поясами
        document.addEventListener('resume', function () {
            if ((new Date()).getTimezoneOffset() !== localTimezoneOffset) {
                window.app.restart();
            }
        });

        // поддержка тестового режима
        LocalStorageWrapper.getItemAsync('god', 'false')
            .then(val => {
                window.isGod = (val === 'true');
                if (!window.isGod) {
                    return LocalStorageWrapper.setItemAsync('god', 'false');
                }
            })
            // news last seen timestamp
            .then(() => LocalStorageWrapper.getItemAsync('newsSeenIDs', '[]'))
            // при первой загрузке загружаем запомненный язык
            .then(() => LocalStorageWrapper.getItemAsync('language'))
            .then(val => {
                window.language = val;
                console.log('User Chosen Language: ' + window.language);
                window.app.onLanguageReady.call(thisPtr);
            })
            .catch(error => {
                navigator.globalization.getPreferredLanguage(
                    lang => {
                        window.language = lang.value.substr(0, 2);
                        console.log('Platform Language: ' + window.language);
                        LocalStorageWrapper.setItemAsync('language', window.language);
                        window.app.onLanguageReady.call(thisPtr);
                    },
                    () => {
                        console.error('Failed to get current language!');
                        window.language = 'ru-RU';
                        LocalStorageWrapper.setItemAsync('language', window.language);
                        window.app.onLanguageReady.call(thisPtr);
                    }
                );
            });
    },

    onLanguageReady: function () {
        let thisPtr = this;

        switch (window.language) {
            //case 'en':
            case 'ru':
                break;
            default:
                window.language = 'ru';
        }

        html10n.localize(window.language);
        html10n.bind('localized', function () {
            html10n.unbind('localized');
            document.documentElement.lang = html10n.getLanguage();
            document.documentElement.dir  = html10n.getDirection();
            window.app.initializeApp.call(thisPtr);
        });
    },

    initializeApp: function() {
        let thisPtr = this;

        Handlebars.registerHelper('html10n', function (str, opts) {
            return (html10n != undefined) ? html10n.get(str) : str;
        });

        Handlebars.registerHelper('date', function (timestamp, opts) {
            let date;
            if (!(timestamp instanceof Date)) {
                date = new Date(timestamp * 1000);
            } else {
                date = timestamp;
            }
            return html10n.get('date[format]', {
                day: date.getDate().toString(),
                month: html10n.get('date[month][' + date.getMonth() + '][full]'),
                year: date.getFullYear(),
                hours: date.getHours().toString().paddingLeft("00"),
                minutes: date.getMinutes().toString().paddingLeft("00"),
                seconds: date.getSeconds().toString().paddingLeft("00")
            });
        });

        Handlebars.registerHelper('if_eq', function (a, b, opts) {
            if (a == b) {
                return opts.fn(this);
            } else {
                return opts.inverse(this);
            }
        });

        Handlebars.registerHelper('if_eq_strict', function (a, b, opts) {
            if (a === b) {
                return opts.fn(this);
            } else {
                return opts.inverse(this);
            }
        });

        // Boolean logical operators
        // https://stackoverflow.com/a/31632215/2175025
        Handlebars.registerHelper('and', () => Array.prototype.slice.call(arguments).every(Boolean));
        Handlebars.registerHelper('or', () => Array.prototype.slice.call(arguments, 0, -1).some(Boolean));
        Handlebars.registerHelper('not', (a) => !a);

        Handlebars.registerHelper('join', function (val, params) {
            return [].concat(val).join(params.hash.delimiter);
        });
        Handlebars.registerHelper('speakers', function (arr, curr, role) {
            let tmp = [];
            arr.forEach(function (speaker) {
                //let speakerName = speaker['name_' + window.language];
                let speakerName;
                if (
                    (role === 'chairman')
                    && (typeof speaker['lastName_' + window.language] === typeof '')
                    && (speaker['lastName_' + window.language].trim() !== '')
                ) {
                    speakerName = speaker['lastName_' + window.language] + ' ' + speaker['firstName_' + window.language];
                } else {
                    speakerName = speaker['firstName_' + window.language];
                }
                if ((role === 'speaker') && speaker['city']) {
                    speakerName += ' (' + speaker['city'] + ')';
                }
                if (speaker === curr) {
                    tmp.push('<strong>' + speakerName + '</strong>');
                } else {
                    tmp.push(speakerName);
                }
            });
            return tmp.join(', ');
        });
        Handlebars.registerHelper('localized_prop', function (params) {
            return params.hash.object[params.hash.propName + '_' + window.language];
        });
        Handlebars.registerHelper('format_time', function (date) {
            return date.getHours() + ':' + date.getMinutes().toString().paddingLeft("00");
        });
        Handlebars.registerHelper('format_date', function (date) {
            return _("date[format]", {
                day: date.getDate().toString(),
                month: _("date[month][" + date.getMonth() + "][full]")
            });
        });

        // Initialize app
        window.myApp = new Framework7({
            // Enable Material theme for Android device only
            material: !!window.isAndroid,
            // Enable Template7 pages
            template7Pages: false,
            materialRipple: false,

            animateNavBackIcon: true,
            pushState: true, //при переходе между экранами, чтобы работала кнопка back на android
            modalTitle: "АКР 2018",
            modalButtonCancel: "Отмена", //текст Cancel кнопки
            swipePanel: 'left', //включаем левого меню свайпом
            swipeBackPage: false
        });

        let changeStyle = function (styleName) {
            $$("#pageStyle").attr("href", 'lib/framework7/css/framework7.' + styleName + '.min.css');
            $$("#pageStyleColours").attr("href", 'lib/framework7/css/framework7.' + styleName + '.colors.min.css');

            let $pages = $$('.page');

            if (styleName === 'material') {
                window.isAndroid = true;
                window.isIos = false;

                $$.each($pages, function (idx, elem) {
                    let $page = $$(elem);
                    if (idx == 0) {
                        $page.prepend($$('.navbar'));
                    } else {
                        window.app.cloneBarsToPage($page);
                    }
                });

                $$('.pages')
                    .addClass('navbar-fixed')
                    .removeClass('navbar-through');
            } else if (styleName === 'ios') {
                window.isAndroid = false;
                window.isIos = true;

                let $subnavbar = $$('.page.page-on-center').find('.subnavbar');
                if ($subnavbar.length) {
                    $$($$('.navbar')[0]).append($subnavbar);
                }

                $$('.navbar:not([id])').remove();

                $$('.view').prepend($$('.navbar'));

                $$('.pages')
                    .removeClass('navbar-fixed')
                    .addClass('navbar-through');
            }

            window.style = styleName;

            $$('body').attr('class', styleName);
        };

        let applyLanguage = function () {
            let context = {
                language: window.language,
                style: window.style
            };

            let navbarTpl = Handlebars.compile($$('#navbar-tpl').html());
            $$('#navbar-tpl').parent().html(navbarTpl(context));

            let splashTpl = Handlebars.compile($$('#splash-tpl').html());
            $$('#splash-tpl').parent().html(splashTpl(context));
        };

        // при первой загрузке загружаем запомненный css
        LocalStorageWrapper.getItemAsync('css', window.style)
            .then(style => changeStyle(style));

        applyLanguage(window.language);

        $$('body').css('display', '');

        window.plugins.spinnerDialog.hide();

        thisPtr.compileHandlebarsTemplates();
    },

    compileHandlebarsTemplates: function () {
        window.compiledTpls = {};

        let templates = [
            cordova.file.applicationDirectory + 'www/programme/templates/sessionCard.hbs',
            cordova.file.applicationDirectory + 'www/programme/templates/lectureCard.hbs',
            cordova.file.applicationDirectory + 'www/templates/news.hbs',
            cordova.file.applicationDirectory + 'www/templates/newsSingle.hbs'
        ];
        
        let promise = templates.reduce(function (prevValue, curItem) {
            return prevValue.then(function () {
                return new Promise(function (resolve) {
                    let winKey = /\/([\w\d\-_]+)\.hbs/g.exec(curItem)[1];
                    window.app.compileTpl(curItem, winKey, resolve);
                });
            })
        }, Promise.resolve());

        promise.then(function () {
            window.app.startApp();
        });
    },

    compileTpl: function (fileName, winKey, callback) {
        window.resolveLocalFileSystemURL(fileName, function (tplFileEntry) {
            tplFileEntry.file(function (tplFile) {
                let fileReader = new FileReader();
                fileReader.onloadend = function (controller) {
                    //console.log(this.result);
                    window.compiledTpls[winKey] = Handlebars.compile(this.result);
                    callback();
                };
                fileReader.readAsText(tplFile);
            });
        });
    },

    startApp: function () {
        let $statusText = $$('.login-screen').find('.status-text');

        let verifyTimestamp, onNetError, onNetErrorIgnored, onDownloaded, onExists, onNotFound, onNotUnpacked, onUnzipped, readJSON, onError, onProgress, unzipBundled;
        let cacheDirEntry, dataDirEntry, unpackedDirEntry, zipFileEntry;

        onProgress = function (progressEvent) {
            window.myApp.setProgressbar('.login-screen .progressbar', progressEvent.loaded / progressEvent.total * 100);
        };
        onError = function(msg) {
            console.log('Error:');
            console.log(msg);
            let descriptiveText = msg;
            if (msg.code) {
                switch (msg.code) {
                    case FileError.NOT_FOUND_ERR:
                        descriptiveText = 'NOT_FOUND_ERR';
                        break;
                    case FileError.SECURITY_ERR:
                        descriptiveText = 'SECURITY_ERR';
                        break;
                    case FileError.ABORT_ERR:
                        descriptiveText = 'ABORT_ERR';
                        break;
                    case FileError.NOT_READABLE_ERR:
                        descriptiveText = 'NOT_READABLE_ERR';
                        break;
                    case FileError.ENCODING_ERR:
                        descriptiveText = 'ENCODING_ERR';
                        break;
                    case FileError.NO_MODIFICATION_ALLOWED_ERR:
                        descriptiveText = 'NO_MODIFICATION_ALLOWED_ERR';
                        break;
                    case FileError.INVALID_STATE_ERR:
                        descriptiveText = 'INVALID_STATE_ERR';
                        break;
                    case FileError.SYNTAX_ERR:
                        descriptiveText = 'SYNTAX_ERR';
                        break;
                    case FileError.INVALID_MODIFICATION_ERR:
                        descriptiveText = 'INVALID_MODIFICATION_ERR';
                        break;
                    case FileError.QUOTA_EXCEEDED_ERR:
                        descriptiveText = 'QUOTA_EXCEEDED_ERR';
                        break;
                    case FileError.TYPE_MISMATCH_ERR:
                        descriptiveText = 'TYPE_MISMATCH_ERR';
                        break;
                    case FileError.PATH_EXISTS_ERR:
                        descriptiveText = 'PATH_EXISTS_ERR';
                        break;
                }
            }
            window.myApp.alert('Всё плохо:\n\n' + descriptiveText);
        };
        onNetError = function (callbackRetry, callbackIgnore, zipEntry) {
            $$('.login-screen .btnCancel-container').hide();

            window.myApp.modal({
                title: _('net error title'),
                text: _('net error text'),
                buttons: [
                    {
                        text: _('retry'),
                        close: true,
                        onClick: callbackRetry.bind(null, zipEntry)
                    },
                    {
                        text: _('ignore'),
                        close: true,
                        onClick: callbackIgnore.bind(null, zipEntry)
                    }
                ]
            });
        };

        let oldUnpackedDirBackedUp = false;

        onDownloaded = function (entry) {
            $$('.login-screen .btnCancel-container').hide();

            console.log("Downloaded: " + entry.toURL());

            let onUnpackedDirBackedUp = function () {
                onExists(entry);
            };

            cacheDirEntry.getDirectory('unpacked', {}, function(dirEntry) {
                console.log('Backing old unpacked dir up for a case ZIP is corrupted');
                dirEntry.moveTo(cacheDirEntry, 'unpacked.old', function() {
                    oldUnpackedDirBackedUp = true;
                    onUnpackedDirBackedUp();
                });
            }, onUnpackedDirBackedUp);
        };

        unzipBundled = function (zipEntry) {
            // fallback to bundled ZIP
            $statusText.text(_('preparing first'));
            $$('.login-screen .progressbar-infinite').show();
            $$('.login-screen .progressbar').hide();

            window.resolveLocalFileSystemURL(cordova.file.applicationDirectory, function (appDirEntry) {
                appDirEntry.getDirectory('www', {}, function (wwwDirEntry) {
                    wwwDirEntry.getDirectory('res', {}, function (resDirEntry) {
                        resDirEntry.getFile('conf-3.zip', {exclusive: false}, function (bundledZipFileEntry) {
                            bundledZipFileEntry.copyTo(zipEntry.filesystem.root, 'conf.zip', onDownloaded.bind(null, zipEntry), onError);
                        }, onError);
                    })
                });
            }, onError);
        };

        onNetErrorIgnored = function (zipEntry) {
            console.log('Net Error ignored');
            LocalStorageWrapper.getItemAsync('packVer')
                .then(val => {
                    console.log('Using previously downloaded file');
                    onExists(zipEntry);
                })
                .catch(error => {
                    console.log('Using bundled file');
                    unzipBundled(zipEntry);
                });
        };

        onExists = function (zipEntry) {
            console.log('onExists invoked');
            zipFileEntry = zipEntry;
            cacheDirEntry.getDirectory('unpacked', {}, function() {
                console.log('Unpacked already');
                onUnzipped(0);
            }, onNotUnpacked);
        };

        onNotFound = function () {
            console.log('onNotFound invoked');
            FileSystemWrapper.removeFileAsync(dataDirEntry, 'conf.zip', true)
                .then(() => FileSystemWrapper.openFileAsync(dataDirEntry, 'conf.zip', {
                    create: true,
                    exclusive: false,
                }))
                .then(fileEntry => {
                    console.log('Downloading to: ' + fileEntry.toURL());
                    let srcUrl = `https://schedule.vandco.ru/upload/conf-3.zip?rand=${Math.random()}`;
                    console.log('Downloading from: ' + srcUrl);

                    let fileTransfer = new FileTransfer();
                    let fileURL = fileEntry.toURL();

                    fileTransfer.onprogress = onProgress;
                    $statusText.text(_('downloading'));
                    fileTransfer.download(
                        srcUrl,
                        fileURL,
                        onDownloaded,
                        function() { onNetError(onNotFound, onNetErrorIgnored, fileEntry); }
                    );

                    $$('.login-screen .btnCancel-container').show();
                    $$('.login-screen .btnCancel').on('click', function (e) {
                        fileTransfer.abort();
                    });
                })
                .catch(onError);
        };

        onNotUnpacked = function () {
            console.log('onNotUnpacked invoked');
            console.log('Data:');
            console.log(dataDirEntry);
            cacheDirEntry.getDirectory('unpacked', {
                create: true
            }, function (dirEntry) {
                unpackedDirEntry = dirEntry;
                $$('.login-screen .progressbar-infinite').hide();
                $$('.login-screen .progressbar').show();
                $statusText.text(_('unpacking'));
                window.zip.unzip(zipFileEntry.toURL(), unpackedDirEntry.toURL(), onUnzipped, onProgress);
            }, onError);
        };

        onUnzipped = function (status) {
            console.log('onUnzipped invoked');
            if (status == 0) {
                console.log('Unzipped: ', cordova.file.cacheDirectory);
                if (oldUnpackedDirBackedUp) {
                    console.log('Deleting old unzipped dir');
                    cacheDirEntry.getDirectory('unpacked.old', {}, function(oldDirEntry) {
                        oldDirEntry.removeRecursively();
                    });
                }

                $$('.login-screen .progressbar-infinite').hide();
                $$('.login-screen .progressbar').show();
                $statusText.text(_('processing'));

                cacheDirEntry.getDirectory('unpacked', {}, function (dirEntry) {
                    unpackedDirEntry = dirEntry;
                    readJSON();
                }, onError);
            } else {
                let notifyUser = function (wasBackup) {
                    if (wasBackup) {
                        onNetError(onNotFound, onUnzipped.bind(null, 0));
                    } else {
                        dataDirEntry.getFile('conf.zip', {
                            create: true,
                            exclusive: false
                        }, function (fileEntry) {
                            onNetError(onNotFound, unzipBundled, fileEntry);
                        });
                    }
                };

                if (oldUnpackedDirBackedUp) {
                    console.log('Deleting new failed unpacked dir');

                    let restore = function () {
                        console.log('Restoring backup');
                        cacheDirEntry.getDirectory('unpacked.old', {}, function (dirEntry) {
                            dirEntry.moveTo(cacheDirEntry, 'unpacked', function() {
                                oldUnpackedDirBackedUp = false;
                                notifyUser(true);
                            });
                        });
                    };

                    cacheDirEntry.getDirectory('unpacked', {}, function (dirEntry) {
                        dirEntry.removeRecursively(restore);
                    }, restore);
                } else {
                    notifyUser(false);
                }
            }
        };

        readJSON = function () {
            console.log('Reading main.json');
            unpackedDirEntry.getFile('main.json', {}, function (jsonFileEntry) {
                jsonFileEntry.file(function (jsonFile) {
                    let fileReader = new FileReader();
                    fileReader.onloadend = function () {
                        //console.log(this.result);
                        let schedule = JSON.parse(this.result);
                        console.log(schedule);

                        window.menuItems = schedule.dynamicContent.menu;
                        window.infoPagesMetadata = schedule.dynamicContent.infoPages;
                        window.infoPages = {};
                        window.infoPagesMetadata.forEach(function(elem) {
                            window.infoPages[elem.id] = elem;
                        });

                        window.menu = [];
                        window.menuItems.forEach(function(elem) {
                            let menuItem = Object.assign({}, elem);
                            if (elem.info_page_id !== null) {
                                menuItem['icon_mime'] = window.infoPages[elem.info_page_id].icon_mime;
                                menuItem['icon_b64'] = window.infoPages[elem.info_page_id].icon_b64;
                                menuItem['title_ru'] = window.infoPages[elem.info_page_id].title_ru;
                                menuItem['title_en'] = window.infoPages[elem.info_page_id].title_en;
                            }
                            window.menu.push(menuItem);
                        });

                        window.mapObjects = schedule.dynamicContent.mapObjects;

                        window.news = {};
                        schedule.dynamicContent.news.forEach(function(newsItem) {
                            newsItem.published_at = new Date(newsItem.published_at * 1000);
                            window.news[newsItem.id] = newsItem;
                        });

                        LocalStorageWrapper['packVer'] = schedule.version;

                        let day1_start = new Date(2018, 7-1, 3);
                        let day1_end = new Date(2018, 7-1, 3, 23, 59, 59);
                        let day2_start = new Date(2018, 7-1, 4);
                        let day2_end = new Date(2018, 7-1, 4, 23, 59, 59);

                        Schedule.init(schedule, {
                            sessions: {
                                tab1: function(session) {
                                    return (
                                        (session.start.getTime() >= day1_start.getTime())
                                        && (session.start.getTime() <= day1_end.getTime())
                                    );
                                },
                                tab2: function(session) {
                                    return (
                                        (session.start.getTime() >= day2_start.getTime())
                                        && (session.start.getTime() <= day2_end.getTime())
                                    );
                                }
                            }
                        }, onProgress, function(ok, filteredObjects) {
                            //console.log('Tabbed Schedule:');
                            //console.log(filteredObjects);
                            ProgrammeController().treeByTabs = filteredObjects.sessions;

                            LocalStorageWrapper.getItemAsync('myAgenda', JSON.stringify([]))
                                .then(stringedData => {
                                    console.log('myAgenda read successfully');
                                    try {
                                        let myAgendaData = JSON.parse(stringedData);
                                        let saveMyAgenda = (toSave) => {
                                            LocalStorageWrapper['myAgenda'] = JSON.stringify(toSave);
                                        };
                                        MyAgenda.init(myAgendaData, saveMyAgenda);

                                        window.app.initializeMainView.call(window.app);
                                    } catch (e) {
                                        console.error('myAgenda parsing error');
                                        console.error(e);
                                        console.error(stringedData);
                                    }
                                })
                                .catch(error => {
                                    console.error('myAgenda reading error');
                                    console.error(error);
                                    throw error;
                                });
                        });
                    };
                    fileReader.readAsText(jsonFile);
                }, onError);
            }, onError);
        };

        verifyTimestamp = function () {
            console.log('verifyTimestamp invoked');
            window.app.verifyScheduleTimestamp(
                // onNoTimestamp
                function () {
                    console.log('onNoTimestamp invoked');
                    $$('.login-screen .progressbar-infinite').hide();
                    $$('.login-screen .progressbar').show();

                    onNotFound();
                },

                // onBeginVerifying
                function () {
                    $statusText.text(_('connecting'));
                },
                // onSuccessVerifying
                function () {
                    $$('.login-screen .progressbar-infinite').hide();
                    $$('.login-screen .progressbar').show();
                },
                // onUpdated
                function () {
                    console.log('onUpdated invoked');
                    onNotFound();
                },
                // onActual
                function () {
                    console.log('onActual invoked');
                    dataDirEntry.getFile('conf.zip', {}, onExists, onNotFound);
                },
                // onError
                function () {
                    console.log('AJAX Error');
                    dataDirEntry.getFile('conf.zip', {}, function (fileEntry) {
                        onNetError(verifyTimestamp, onExists, fileEntry);
                    }, onNotFound);
                }
                // onComplete
            );
        };

        window.resolveLocalFileSystemURL(cordova.file.dataDirectory, function (dirEntry) {
            dataDirEntry = dirEntry;

            window.resolveLocalFileSystemURL(cordova.file.cacheDirectory, function (dirEntry) {
                cacheDirEntry = dirEntry;
                verifyTimestamp();
            }, onError);
        }, onError);
    },

    verifyScheduleTimestamp: function(
        onNoTimestamp,
        onBeginVerifying,
        onSuccessVerifying,
        onUpdated,
        onActual,
        onError,
        onComplete
    ) {
        LocalStorageWrapper.setItemAsync('lastVerifiedTimestamp', (new Date()).getTime())
            .then(() => {
                console.log('Checking packVer');
                return LocalStorageWrapper.getItemAsync('packVer');
            })
            .then(packVer => {
                console.log(packVer);

                if (typeof onBeginVerifying === typeof (function(){})) onBeginVerifying();

                console.log('Requesting the server');
                $$.ajax({
                    url: 'https://schedule.vandco.ru/schedule/latestVersion/3',
                    crossDomain: true,
                    dataType: 'jsonp',
                    timeout: 5000,
                    cache: false,
                    success: function(ver) {
                        console.log('Server responded:');
                        console.log(ver);

                        if (typeof onSuccessVerifying === typeof (function(){})) onSuccessVerifying();

                        if (ver != packVer) {
                            if (typeof onUpdated === typeof (function(){})) onUpdated();
                        } else {
                            if (typeof onActual === typeof (function(){})) onActual();
                        }
                    },
                    error: onError,
                    complete: onComplete
                });
            })
            .catch(error => {
                console.log('No packVer');
                if (typeof onNoTimestamp === typeof (function(){})) onNoTimestamp();
                if (typeof onComplete === typeof (function(){})) onComplete();
            });
    },

    initializeMvcLinks: function () {
        $$(document).on('click', 'a[data-controller]', function(e) {
            let $a = $$(e.target);
            while (!$a.is('a[href]')) {
                $a = $a.parent();
            }

            let controllerName = $a.attr('data-controller');
            controllerName = controllerName.substr(0, 1).toUpperCase() + controllerName.substr(1) + 'Controller';
            console.log('MVC Call: ' + controllerName);
            let query = $a.attr('data-query');
            if (query) {
                console.log('MVC Query: ' + query);
                try {
                    query = JSON.parse(query);
                    console.log('MVC Parsed JSON-Query:');
                    console.log(query);
                } catch (e) {
                    console.log('Unparsable JSON');
                    throw e;
                }
            }
            let ignoreCache = $a.is('[data-ignore-cache]');
            if (controllerName in window.controllers) {
                e.preventDefault();
                window.controllers[controllerName]().load(query, ignoreCache);
            }
        });
    },

    initializeMainView: function() {
        let thisPtr = this;

        // Add view
        window.mainView = window.myApp.addView('.view-main', {
            // Because we want to use dynamic navbar, we need to enable it for this view:
            // (Material doesn't support it but don't worry about it,
            // F7 will ignore it for Material theme)
            dynamicNavbar: true,
            domCache: true //чтобы навигация работала без сбоев и с запоминанием scroll position в длинных списках
        });

        console.log(window.myApp);
        console.log(window.mainView);

        window.app.initializeMvcLinks();

        Promise.all([
            LocalStorageWrapper.getItemAsync('packVer'),
            LocalStorageWrapper.getItemAsync('lastVerifiedTimestamp'),
            LocalStorageWrapper.getItemAsync('newsSeenIDs'),
        ]).then(([packVer, lastVerifiedTimestamp, newsSeenIDs]) => {
            lastVerifiedTimestamp /= 1000;
            newsSeenIDs = JSON.parse(newsSeenIDs);

            $$(document).on('pageBeforeInit', function(e) {
                let context = {
                    language: window.language,
                    style: window.style,
                    god: window.isGod,
                    packVer: packVer,
                    lastVerifiedTimestamp: lastVerifiedTimestamp
                };

                let page = e.detail.page;
                //console.log(page);
                let $pageContainer = $$(page.container);
                let $tpls = $pageContainer.find('script[type="text/template"]');
                $tpls.each(function () {
                    let $tpl = $$(this);
                    let pageTpl = Handlebars.compile($tpl.html());
                    let $compiledTpl = $$(pageTpl(context));
                    $compiledTpl.insertBefore($tpl);
                    $tpl.remove();
                });
            });

            if (window.isIos) {
                $$(document).on('navbarBeforeInit', function(e) {
                    let context = {
                        language: window.language,
                        style: window.style,
                        god: window.isGod
                    };

                    let $navbar = $$(e.detail.navbar.innerContainer);
                    let tpl = Handlebars.compile($navbar.html());
                    $navbar.html(tpl(context));
                });
            }

            $$(document).on('pageInit', function (e) {
                $$(document).off('pageInit');
                window.app.onMainViewReady.call(thisPtr);
            });

            let unreadNewsCount = 0;
            console.log('News Seen IDs:');
            console.log(newsSeenIDs);
            for (let key in window.news) {
                if (!newsSeenIDs.includes(key)) unreadNewsCount++;
            }
            window.app.compileTpl(cordova.file.applicationDirectory + 'www/templates/menu.hbs', 'left-menu', function() {
                let compiledMenuHtml = window.compiledTpls['left-menu']({
                    menu: window.menu,
                    language: window.language,
                    unreadNewsCount: unreadNewsCount
                });
                $$('#left-menu-tpl').html(compiledMenuHtml);

                window.myApp.openPanel('left', false);

                $$(document).on('click', '[data-close-panel]', function(e) {
                    setTimeout(function() {
                        window.myApp.closePanel();
                    }, 500);
                });

                ProgrammeController().load({
                    tab: 0
                });
            });
        });
    },

    onMainViewReady: function() {
        $$(document).on('pageInit', function (e) {
            // Get page data from event data
            let page = e.detail.page;

            if (window.isAndroid) {
                window.app.cloneBarsToPage($$(page.container));
            }
        });

        window.myApp.closeModal('.login-screen');

        if (typeof window.checkUpdatesIntervalID !== typeof undefined) {
            clearInterval(window.checkUpdatesIntervalID);
        }
        window.currentlyCheckingUpdates = false;
        window.checkUpdatesIntervalID = setInterval(function () {
            if (window.currentlyCheckingUpdates)
                return;

            window.currentlyCheckingUpdates = true;

            let showNotification = function() {
                window.lockedBackgroundVerification = true;
                window.myApp.addNotification({
                    title: _('schedule updated title'),
                    message: _('schedule updated'),
                    //hold: 15000,
                    button: {
                        text: _('close')
                    },
                    onClick: function () {
                        window.plugins.spinnerDialog.show(_('please wait'), _('please wait'), true);
                        window.app.restart();
                    },
                    onClose: function () {
                        setTimeout(function () {
                            window.lockedBackgroundVerification = false;
                        }, 2000);
                    }
                });
            };

            if (!window.lockedBackgroundVerification) {
                //let interval = 10 * 60 * 1000;
                let interval = 10 * 1000;
                if (window.isGod) {
                    //interval = 5 * 1000;
                }

                LocalStorageWrapper.getItemAsync('lastVerifiedTimestamp').then(lastVerifiedTimestamp => {
                    //console.log('Need to really verify timestamp? (Passed interval?)');
                    if ((new Date()).getTime() > parseInt(lastVerifiedTimestamp) + interval) {
                        //console.log('Yes, passed interval, verifying timestamp with the server');
                        window.app.verifyScheduleTimestamp(
                            function () {}, // onNoTimestamp
                            function () {}, // onBeginVerifying
                            function () {}, // onSuccessVerifying
                            showNotification, // onUpdated
                            () => {}, // onActual
                            () => {}, // onError
                            () => { // onComplete
                                window.currentlyCheckingUpdates = false;
                            }
                        );
                    } else {
                        //console.log('No, skipping verifying timestamp with the server');
                        window.currentlyCheckingUpdates = false;
                    }
                });
            } else {
                window.currentlyCheckingUpdates = false;
            }
        }, 1000);
    },

    $getNavbar: function($page) {
        if (window.isAndroid) {
            return $$('#navbar');
        } else {
            let $transitioningNavbar = $$('#navbar').find('.navbar-from-left-to-center');
            if ($transitioningNavbar.length) {
                return $transitioningNavbar;
            } else {
                return $$('#navbar').find('.navbar-inner:last-child');
            }
        }
    },

    cloneBarsToPage: function($page) {
        /*let $navbar = $$($$('#navbar')[0].cloneNode(true)).removeAttr('id');
        let $toolbar = $$($$('#toolbar')[0].cloneNode(true)).removeAttr('id');

        $page.prepend($navbar);
        $page.append($toolbar);

        $navbar.find('.center').html($page.attr('data-title'));

        let $subnavbar = $page.find('.subnavbar');
        if ($subnavbar.length) {
            $navbar.append($subnavbar);
        }*/
    },

    resetNavBar: function () {
        window.app.$getNavbar().find('.right').html('');
        //window.app.$getNavbar().find('.left').find('.link.back').remove();
        window.myApp.sizeNavbars('.view-main');
    },

    setTitle: function (titleHTML) {
        window.app.$getNavbar().find('.center').html(titleHTML);
    },

    restart: function () {
        window.location.hash = '';
        window.location.reload();
    }
};

window.app.initialize();