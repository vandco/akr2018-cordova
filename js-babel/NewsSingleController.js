import BaseController from './BaseController';

var _newsSingleController = Object.create(BaseController);
Object.defineProperty(_newsSingleController, '_pageName', {value: 'news-single'});
Object.defineProperty(_newsSingleController, '_pageTitle', {value: 'Новость'});
//Object.defineProperty(_newsSingleController, '_page', {writable: true});
Object.defineProperty(_newsSingleController, 'onInit', {value: function ($pageContainer, page) {
    var context = {
        language: window.language,
        newsObj: window.news[page.query.newsID]
    };
    var compiledHtml = window.compiledTpls.newsSingle(context).trim();
    $pageContainer.find('.content-block').html(compiledHtml);
}});
Object.defineProperty(_newsSingleController, 'onAfterAnimation', {value: function (page) {
    var $navbarRight = window.app.$getNavbar($$('#' + this._pageID)).find('.right');
    var $btnBack = $$('<a href="#" class="link back" />');
    $btnBack.html(_('back')).appendTo($navbarRight);
    window.myApp.sizeNavbars('.view-main');

    this.onBackButton = function() {
        $btnBack.click();
        return false;
    };
}});


let NewsSingleController = () => _newsSingleController._construct();
export default NewsSingleController;
