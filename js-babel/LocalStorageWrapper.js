import NativeStorageError from '../node_modules/cordova-plugin-nativestorage/www/NativeStorageError';

const nativeStorageMainKey = 'cordova-app-config';

let localStorageWrapper = {
    _cachedConfig: null,

    _readConfigAsync() {
        console.log('NativeStorage Full Config required');

        if (this._cachedConfig === null) {
            console.log('No cached NativeStorage Full Config');
            console.log(`Reading NativeStorage config`);
            return (new Promise((resolve, reject) => {
                NativeStorage.getItem(nativeStorageMainKey, resolve, reject);
            }))
                .then(configStringed => {
                    console.log('Retrieved NativeStorage Full Config');
                    console.log(configStringed);
                    this._cachedConfig = JSON.parse(configStringed);
                    return this._cachedConfig;
                })
                .catch(error => {
                    if (error.code === NativeStorageError.ITEM_NOT_FOUND) {
                        console.log('No NativeStorage Full Config');
                        this._cachedConfig = {};
                        return this._cachedConfig;
                    } else {
                        console.error('Unrecoverable error with NativeStorage');
                        console.error(error);
                    }
                });
        } else {
            console.log('Cached NativeStorage Full Config found');
            return Promise.resolve(this._cachedConfig);
        }
    },
    _writeConfigAsync(config) {
        console.log(`Writing NativeStorage config`);
        console.log(config);

        const configStringed = JSON.stringify(config);

        return (new Promise((resolve, reject) => {
            NativeStorage.setItem(nativeStorageMainKey, configStringed, resolve, reject);
        }))
            .then(() => {
                console.log('Written NativeStorage Full Config');
                this._cachedConfig = config;
                console.log('this:');
                console.log(this);
                return config;
            })
            .catch(error => {
                console.error('Unrecoverable error with NativeStorage');
                console.error(error);
            });
    },

    setItemAsync(key, value) {
        console.log(`Setting NativeStorage item ${key} with ${value}`);

        return this._readConfigAsync()
            .then(config => {
                console.log('Successfully read NativeStorage Full Config, modifying...');
                config[key] = value;
                console.log(config);
                //return config;
            //})
            //.then(config => {
                console.log('Modified NativeStorage, writing down');
                return this._writeConfigAsync(config);
            });
    },

    getItemAsync(key, defaultValue) {
        console.log(`Required NativeStorage key: ${key}`);

        return this._readConfigAsync()
            .then(config => {
                if (!(key in config)) {
                    if (typeof defaultValue !== typeof undefined) {
                        config[key] = defaultValue;
                        return this._writeConfigAsync(config);
                    } else {
                        throw new Error(`No ${key} found in NativeStorage, and no fallback value provided!`);
                    }
                } else {
                    return config;
                }
            })
            .then(config => config[key]);
    },

    keysAsync() {
        return this._readConfigAsync()
            .then(config => Object.keys(config));
    },

    removeAsync(key) {
        return this._readConfigAsync()
            .then(config => {
                delete config[key];
                return config;
            })
            .then(config => this._writeConfigAsync(config));
    },
};

let LocalStorageWrapper = new Proxy(localStorageWrapper, {
    set(target, prop, value) {
        if (prop !== '_cachedConfig') {
            target.setItemAsync(prop, value);
        } else {
            target[prop] = value;
        }
        return true;
    }
});

export default LocalStorageWrapper;
