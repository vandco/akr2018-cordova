import BaseController from './BaseController';
import LocalStorageWrapper from './LocalStorageWrapper';

var _questionsController = Object.create(BaseController);
Object.defineProperty(_questionsController, '_pageName', {value: 'questions'});
Object.defineProperty(_questionsController, '_pageTitle', {value: 'Задать вопрос'});
Object.defineProperty(_questionsController, 'onInit', {value: function ($pageContainer, page) {
    var $contentBlock = $pageContainer.find('.content-block');

    var $fromName = $pageContainer.find('[name="fromName"]');
    var $fromEmail = $pageContainer.find('[name="fromEmail"]');
    var $textArea = $pageContainer.find('textarea');
    var $btnSubmit = $contentBlock.find('button[type="submit"]');

    // fill in pre-saved contacts
    LocalStorageWrapper.getItemAsync('contacts')
        .then(contacts => {
            try {
                contacts = JSON.parse(contacts);

                if (typeof contacts.name !== typeof undefined) {
                    $fromName.val(contacts.name);
                }

                if (typeof contacts.email !== typeof undefined) {
                    $fromEmail.val(contacts.email);
                }
            } catch(e) {
                LocalStorageWrapper.removeAsync('contacts');
            }
        });

    this.on('click', 'button[type="submit"]', (function(e) {
        e.preventDefault();

        $$('.has-error').removeClass('has-error');

        if (
            !$fromName.val().trim().length
            || !$textArea.val().trim().length
        ) {
            window.myApp.alert(_('required fields empty'));

            if (!$fromName.val().trim().length) {
                $fromName.parent().parent().addClass('has-error');
            }

            if (!$textArea.val().trim().length) {
                $textArea.parent().parent().addClass('has-error');
            }

            return;
        }

        var $preloader = $$('<span />');
        $preloader.addClass('preloader').css('width', '50px').css('height', '50px').html($$('#tpl-preloader').html());
        $preloader.insertAfter($btnSubmit);
        $btnSubmit.hide();
        window.plugins.spinnerDialog.show(_('sending question'), _('please wait'), true);

        this.submitQuestion({
            fromName: $fromName.val(),
            fromEmail: $fromEmail.val(),
            question: $textArea.val()
        }, function() {
            window.myApp.alert(_('asked ok'));

            let contacts = {
                name: $fromName.val(),
                email: $fromEmail.val()
            };
            LocalStorageWrapper.setItemAsync('contacts', JSON.stringify(contacts));

            $preloader.remove();
            $btnSubmit.show();
            window.plugins.spinnerDialog.hide();
        }, function(data) {
            var decodedErrors;
            try {
                decodedErrors = JSON.parse(data);
                var errorMsg = _('ask fields errors') + ' <br />\n<br />\n';
                var errorFields = [];
                decodedErrors.forEach(function(errObj) {
                    for (var fieldCode in errObj) {
                        var errorField = '';
                        if (fieldCode === 'fromName') {
                            errorField = '<b>' + _('your name') + '</b>: ';
                        }
                        if (fieldCode === 'fromEmail') {
                            errorField = '<b>' + _('your email') + '</b>: ';
                        }
                        if (fieldCode === 'question') {
                            errorField = '<b>' + _('your question here') + '</b>: ';
                        }

                        errorField += errObj[fieldCode];
                        errorFields.push(errorField);
                    }
                });

                errorMsg += errorFields.join(' <br />\n');
                window.myApp.alert(errorMsg);
            } catch(e) {
                window.myApp.alert(_('ask failed'));
            }

            $preloader.remove();
            $btnSubmit.show();
            window.plugins.spinnerDialog.hide();
        }, function() {
            window.myApp.alert(_('ask error'));
            $preloader.remove();
            $btnSubmit.show();
            window.plugins.spinnerDialog.hide();
        });
    }).bind(this));
}});
Object.defineProperty(_questionsController, 'onAfterAnimation', {value: function (page) {
    var $navbarRight = window.app.$getNavbar($$('#' + this._pageID)).find('.right');
    //var $btnBack = $$('<a href="#" class="link back" />');
    //$btnBack.html(_('back')).appendTo($navbarRight);
    window.myApp.sizeNavbars('.view-main');

    /*this.onBackButton = function() {
        $btnBack.click();
        return false;
    };*/
}});

Object.defineProperty(_questionsController, 'submitQuestion', {value: function(data, callbackOnSuccess, callbackOnError, callbackOnConnectivityFailure) {
    $$.ajax({
        url: 'http://akr-congress2018.ru/mobile-app/askQuestion',
        //crossDomain: true,
        dataType: 'json',
        timeout: 5000,
        method: 'post',
        data: data,
        success: function(data) {
            console.log(data);
            callbackOnSuccess();
        },
        error: function (xhr, status) {
            console.log(xhr);
            console.log(status);
            if (status === 'timeout') {
                callbackOnConnectivityFailure();
            } else {
                callbackOnError(xhr.responseText);
            }
        }
    });
}});


let QuestionsController = () => _questionsController._construct();
export default QuestionsController;
