import BaseController from './BaseController';
import PositionError from '../node_modules/cordova-plugin-geolocation/www/PositionError';

const placeID = 'ChIJV48eQh9JtUYRNXcb1Mp-Uz4';

var RedMarkerIcon = L.Icon.extend({
    options: {
        iconUrl: 'img/icons/marker-red.png',
        iconRetinaUrl: 'img/icons/marker-red@2x.png',
        iconSize:    [34, 41],
        iconAnchor:  [17, 41],
        popupAnchor: [1, -34]
    }
});
var YellowMarkerIcon = L.Icon.extend({
    options: {
        iconUrl: 'img/icons/marker-yellow.png',
        iconRetinaUrl: 'img/icons/marker-yellow@2x.png',
        iconSize:    [34, 41],
        iconAnchor:  [17, 41],
        popupAnchor: [1, -34]
    }
});
var GreenMarkerIcon = L.Icon.extend({
    options: {
        iconUrl: 'img/icons/marker-green.png',
        iconRetinaUrl: 'img/icons/marker-green@2x.png',
        iconSize:    [34, 41],
        iconAnchor:  [17, 41],
        popupAnchor: [1, -34]
    }
});

var redMarkerIcon = function (options) {
    return new RedMarkerIcon(options);
};
var yellowMarkerIcon = function (options) {
    return new YellowMarkerIcon(options);
};
var greenMarkerIcon = function (options) {
    return new GreenMarkerIcon(options);
};

var createMarker = function (leafletMap, point, iconFactory, popupContent) {
    var marker = L.marker(point, {icon: iconFactory()})
        .addTo(leafletMap)
        .bindPopup(popupContent)
        .on('mousedown click', function (e) {
            marker.openPopup();
        });
    return marker;
};

var GeolocationMarkerIcon = L.icon({
    iconUrl: 'img/icons/geo-marker.png',
    iconRetinaUrl: 'img/icons/geo-marker@2x.png',
    iconSize: [34, 34]
});

var _mapController = Object.create(BaseController);
Object.defineProperty(_mapController, '_pageName', {value: 'map'});
Object.defineProperty(_mapController, '_pageTitle', {value: 'Карта'});
Object.defineProperty(_mapController, '_geolocationWatchID', {writable: true});
Object.defineProperty(_mapController, '_geolocationMarker', {writable: true});
Object.defineProperty(_mapController, '_center', {value: null, writable: true});
Object.defineProperty(_mapController, 'onInit', {value: function ($pageContainer, page) {
    window.myApp.params.swipePanel = false;

    var $pageContent = $pageContainer.find('.page-content');

    var $map = $$('<div />');

    $map.attr('id', this._pageID + '-map');
    $map.css(
        'height',
        (
            $pageContent.height()
            - parseInt($pageContent.css('padding-top'))
            - parseInt($pageContent.css('padding-bottom'))
        )
        + 'px'
    );
    $map.appendTo($pageContent);

    var leafletOptions = {};
    if (window.isAndroid) {
        leafletOptions.zoomControl = false;
    }

    // detect center
    var minLatitude, minLongitude, maxLatitude, maxLongitude;
    window.mapObjects.forEach(function(mapObject) {
        mapObject.latitude = parseFloat(mapObject.latitude);
        mapObject.longitude = parseFloat(mapObject.longitude);

        if ((typeof minLatitude === typeof undefined) || (mapObject.latitude < minLatitude)) {
            minLatitude = mapObject.latitude;
        }
        if ((typeof maxLatitude === typeof undefined) || (mapObject.latitude > maxLatitude)) {
            maxLatitude = mapObject.latitude;
        }
        if ((typeof minLongitude === typeof undefined) || (mapObject.longitude < minLongitude)) {
            minLongitude = mapObject.longitude;
        }
        if ((typeof maxLongitude === typeof undefined) || (mapObject.longitude > maxLongitude)) {
            maxLongitude = mapObject.longitude;
        }
    });
    var centerLatitude = (maxLatitude + minLatitude) / 2;
    var centerLongitude = (maxLongitude + minLongitude) / 2;
    this._center = {latitude: centerLatitude, longitude: centerLongitude};

    var leafletMap = L.map(this._pageID + '-map', leafletOptions).setView([centerLatitude, centerLongitude], 11);
    // TODO: if L.Browser.retina then request @2x URL
    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        attribution: 'Map data &copy; <a data-href="http://openstreetmap.org" class="external">OpenStreetMap</a> contributors',
        maxZoom: 17
    }).addTo(leafletMap);

    /*leafletMap.on('mouseup', function(e) {
        window.myApp.alert("LatLng: " + leafletMap.getCenter().lat + ', ' + leafletMap.getCenter().lng);
    });*/

    leafletMap.attributionControl.setPrefix('<a data-href="http://leafletjs.com" class="external">Leaflet</a>');

    this._geolocationWatchID = navigator.geolocation.watchPosition((function (position) {
        try {
            var radius = position.coords.accuracy / 2;

            if (typeof this._geolocationMarker !== typeof undefined) {
                this._geolocationMarker.remove();
            }
            this._geolocationMarker = L.marker([position.coords.latitude, position.coords.longitude], {icon: GeolocationMarkerIcon});
            this._geolocationMarker.addTo(leafletMap);
        } catch (e) {}
    }).bind(this), function (error) {
        console.log(error);
        var errorDesc;
        switch (error) {
            case PositionError.PERMISSION_DENIED:
                errorDesc = 'permission denied';
                break;
            case PositionError.POSITION_UNAVAILABLE:
                errorDesc = 'position unavailable';
                break;
            case PositionError.TIMEOUT:
                errorDesc = 'timed out';
                break;
        }
        //window.myApp.alert("Location error.");
    }, {
        timeout: 5000,
        enableHighAccuracy: true
    });

    var markers = {};
    window.mapObjects.forEach(function(mapObject) {
        var factoryFunc;
        switch (mapObject.colour) {
            case 'RED': {
                factoryFunc = redMarkerIcon;
                break;
            }

            case 'YELLOW': {
                factoryFunc = yellowMarkerIcon;
                break;
            }

            case 'GREEN': {
                factoryFunc = greenMarkerIcon;
                break;
            }
        }

        markers[mapObject.id] = createMarker(
            leafletMap,
            [mapObject.latitude, mapObject.longitude],
            factoryFunc,
            '<strong>' + mapObject['title_' + window.language] + '</strong>, <br />' + mapObject['popup_content_' + window.language]
        );
    });

    if ('callout' in page.query) {
        if (markers[page.query.callout]) {
            markers[page.query.callout].openPopup();
        }
    }

    this.on('click', 'a[data-href]', function (e) {
        e.preventDefault();

        var url = $$(this).attr('data-href');

        cordova.plugins.browsertab.isAvailable(function(result) {
                if (!result) {
                    window.open(url, "_system");
                } else {
                    cordova.plugins.browsertab.openUrl(
                        url,
                        function(successResp) {},
                        function(failureResp) {
                            window.open(url, "_system");
                        }
                    );
                }
            },
            function(isAvailableError) {
                window.open(url, "_system");
            });

        return false;
    });
}});
Object.defineProperty(_mapController, 'onAfterAnimation', {value: function (page) {
    var timestamp = (new Date()).getTime();
    this._btnMapID = 'btnMap-' + timestamp;

    var $navbarRight = window.app.$getNavbar($$('#' + this._pageID)).find('.right');
    $navbarRight.html('<a href="#" class="link icon-only btnMap" id="' + this._btnMapID + '"><i class="material-icons">open_in_browser</i></a>');
    window.myApp.sizeNavbars('.view-main');

    $navbarRight.find('.btnMap').on('click', (function (e) {
        this.onMapClick();
    }).bind(this));
}});
Object.defineProperty(_mapController, 'onBack', {value: function (page) {
    navigator.geolocation.clearWatch(this._geolocationWatchID);
}});

Object.defineProperty(_mapController, 'onMapClick', {value: function () {
    let title;
    if (Object.keys(window.mapObjects).length) {
        title = window.mapObjects[Object.keys(window.mapObjects)[0]]['title_' + language];
    }

    if (!isAndroid) {
        let appleMapURL = 'http://maps.apple.com/?ll=' + encodeURIComponent(this._center.latitude + ',' + this._center.longitude) + '&z=17';
        if (title) {
            appleMapURL += '&q=' + encodeURIComponent(title);
        }
        window.open(appleMapURL, '_system');
    } else {
        window.open('https://www.google.com/maps/search/?api=1&query=' + encodeURIComponent(this._center.latitude + ',' + this._center.longitude) + `&query_place_id=${placeID}&zoom=17`, '_system');
    }
}});

let MapController = () => _mapController._construct();
export default MapController;
