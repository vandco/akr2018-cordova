import BaseController from './BaseController';
import LocalStorageWrapper from './LocalStorageWrapper';

var _settingsController = Object.create(BaseController);
Object.defineProperty(_settingsController, '_pageName', {value: 'settings'});
Object.defineProperty(_settingsController, 'onInit', {value: function ($pageContainer, page) {
    var thisPtr = this;

    // по клику меняем css файл, меняя ios и material стиль. Выбор запоминаем в localStorage, чтобы при перезапуске приложения загружался нужный css
    this.on('change', 'select[name="style"]', function() {
        thisPtr.onStyleChange($$(this));
        return false;
    });

    // по клику меняем язык. Выбор запоминаем в localStorage, чтобы при перезапуске приложения загружался нужный язык
    this.on('change', 'select[name="language"]', function() {
        thisPtr.onLanguageChange($$(this));
        return false;
    });

    cordova.getAppVersion.getVersionNumber()
        .then((version) => {
            $$('#ver').text(version);
        });

    // тестовый режим :)
    if (!window.isGod) {
        var tapsToBecomeGod = 7;
        this.on('click', '#version-number', function () {
            tapsToBecomeGod--;
            if (!tapsToBecomeGod) {
                tapsToBecomeGod = 7;
                window.myApp.prompt('Введите пароль для перехода в тестовый режим:', 'Тестовый режим', function (password) {
                    if (password == 'iddqd') {
                        LocalStorageWrapper.setItemAsync('god', 'true');
                        window.plugins.spinnerDialog.show('Переход в тестовый режим...', _('please wait'), true);
                        window.app.restart();
                    } else {
                        window.myApp.addNotification({
                            message: 'Неправильный пароль',
                            hold: 3000,
                            closeOnClick: true
                        });
                    }
                })
            } else if (tapsToBecomeGod < 4) {
                window.myApp.addNotification({
                    title: 'Тестовый режим',
                    message: 'Ещё ' + tapsToBecomeGod,
                    hold: 1000,
                    button: {
                        text: _('close')
                    }
                });
            }
        });
    } else {
        this.on('click', '#abortGod', function () {
            window.myApp.confirm('Вам потребуется заново ввести пароль, если пожелаете вернуться в тестовый режим. <br /><br /> Вы уверены, что хотите выйти из тестового режима?', 'Стать простым смертным', function () {
                LocalStorageWrapper.setItemAsync('god', 'false');
                window.plugins.spinnerDialog.show('Выход из тестового режима...', _('please wait'), true);
                window.app.restart();
            })
        });
    }
}});

Object.defineProperty(_settingsController, 'onStyleChange', {value: function ($select) {
    LocalStorageWrapper.setItemAsync("css", $select.val());
    setTimeout(function () {
        window.plugins.spinnerDialog.show(_('skin change in progress'), _('please wait'), true);
        window.app.restart();
    }, 1000);
}});

Object.defineProperty(_settingsController, 'onLanguageChange', {value: function ($select) {
    LocalStorageWrapper.setItemAsync("language", $select.val());
    html10n.localize($select.val());
    html10n.unbind('localized');
    html10n.bind('localized', function() {
        setTimeout(function () {
            window.plugins.spinnerDialog.show(_('language change in progress'), _('please wait'), true);
            window.app.restart();
        }, 1000);
    });
}});

let SettingsController = () => _settingsController._construct();
export default SettingsController;
