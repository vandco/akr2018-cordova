import BaseController from './BaseController';
import LocalStorageWrapper from './LocalStorageWrapper';

var _votingController = Object.create(BaseController);
Object.defineProperty(_votingController, '_pageName', {value: 'voting'});
Object.defineProperty(_votingController, '_pageTitle', {value: 'Голосование'});
Object.defineProperty(_votingController, '_$pageContainer', {writable: true});
Object.defineProperty(_votingController, '_reactiveData', {writable: true, value: {
    poll: {},
    pollTimestamp: null,
    netError: null,
    myVotes: {},
    selectedOptionID: null,
    isSubmitting: false,
    appPaused: false,
}});
Object.defineProperty(_votingController, '_vueApp', {writable: true});
Object.defineProperty(_votingController, '_eventBus', {writable: true});
Object.defineProperty(_votingController, 'onInit', {value: function ($pageContainer, page) {
    var $pageContent = $pageContainer.find('.content-block');
    this._$pageContainer = $pageContainer;

    this.on('click', '.btnRetry', (function (e) {
        this.loadAndProcessPoll();
    }).bind(this));

    this.on('click', '[data-option-id]', (function (e) {
        var $option = $$(e.target);
        while (!$option.is('[data-option-id]')) {
            $option = $option.parent();
        }

        console.log('Option clicked:');
        console.log($option);

        this.onOptionClick($option);
    }).bind(this));

    this.on('click', '.btnVote', (function (e) {
        var $btnVote = $$(e.target);
        while (!$btnVote.is('.btnVote')) {
            $btnVote = $btnVote.parent();
        }

        this.onVoteClick();
    }).bind(this));

    LocalStorageWrapper.getItemAsync('myVotes', '{}')
        .then(myVotes => {
            this.onLocalStorageLoaded(JSON.parse(myVotes));
        });
}});
Object.defineProperty(_votingController, 'onLocalStorageLoaded', {value: function (myVotes) {
    this._reactiveData.myVotes = myVotes;

    var vueAppAndEventBus = window.initVotingApp(this._reactiveData);
    this._vueApp = vueAppAndEventBus.app;
    this._eventBus = vueAppAndEventBus.eventBus;

    this.loadAndProcessPoll();
}});
Object.defineProperty(_votingController, 'onAfterAnimation', {value: function (page) {
    var $navbarRight = window.app.$getNavbar($$('#' + this._pageID)).find('.right');
    window.myApp.sizeNavbars('.view-main');
}});
Object.defineProperty(_votingController, 'onUnload', {value: function () {
    this._vueApp.$destroy();
    this._eventBus.$off();
    this._eventBus.$destroy();
    this._vueApp = null;
    this._eventBus = null;
    console.log('Vue.JS app destroyed');
    console.log(this._reactiveData);
    return true; // to unload the controller completely
}});
Object.defineProperty(_votingController, 'onAppPause', {value: function () {
    this._reactiveData.appPaused = true;
}});
Object.defineProperty(_votingController, 'onAppResume', {value: function () {
    this._reactiveData.appPaused = false;
    if (this._eventBus) {
        this._eventBus.$emit('app-resumed');
    }
}});

Object.defineProperty(_votingController, 'loadPoll', {
    value: (function (callback) {
        this._reactiveData.netError = null;
        this._reactiveData.selectedOptionID = null;

        $$.ajax({
            url: 'http://akr-congress2018.ru/mobile-app/currentPoll',
            //crossDomain: true,
            dataType: 'json',
            timeout: 5000,
            method: 'GET',
            success: (function(data) {
                console.log(data);
                callback(data.poll, data.timestamp);

                this._reactiveData.netError = false;
            }).bind(this),
            error: (function (xhr, status) {
                console.log(xhr);
                console.log(status);

                this._reactiveData.netError = true;
            }).bind(this)
        });
    }).bind(_votingController)
});
Object.defineProperty(_votingController, 'loadAndProcessPoll', {value: function () {
    this.loadPoll((poll, pollTimestamp) => {
        this._reactiveData.poll = poll;
        this._reactiveData.pollTimestamp = pollTimestamp;
    });
}});

Object.defineProperty(_votingController, 'updatePoll', {
    value: (function (poll) {
        this._reactiveData.poll = poll;
    }).bind(_votingController)
});

Object.defineProperty(_votingController, 'onOptionClick', {
    value: (function ($option) {
        var $pageContent = this._$pageContainer.find('.page-content').find('.content-block');
        //$pageContent.find('[data-option-id]').removeAttr('data-checked').find('.item-media .f7-icons').html('circle');
        //$option.attr('data-checked', true).find('.item-media .f7-icons').html('check_round');
        this._reactiveData.selectedOptionID = parseInt($option.attr('data-option-id'));

        //$pageContent.find('.btnVote').removeClass('disabled');
    }).bind(_votingController)
});

Object.defineProperty(_votingController, 'onVoteClick', {
    value: (function () {
        var $pageContent = this._$pageContainer.find('.page-content').find('.content-block');

        //$pageContent.find('.btnVote').remove();

        /*var options = $pageContent.find('[data-option-id]'); // it's some weird hell, not an array
        var selectedOptionID;
        for (var idx = 0; idx < options.length; idx++) {
            var $option = $$(options[idx]);
            var $icon = $option.find('.item-media .f7-icons');
            if (!$option.attr('data-checked')) {
                $icon.css('visibility', 'hidden');
            } else {
                $icon.html('check_round_fill');
                selectedOptionID = parseInt($option.attr('data-option-id'));
            }
        }*/

        this._reactiveData.isSubmitting = true;

        this.submitVote(this._reactiveData.poll.id, this._reactiveData.selectedOptionID, (function(poll, pollTimestamp) {
            this._reactiveData.isSubmitting = false;

            this._reactiveData.poll = poll;
            this._reactiveData.pollTimestamp = pollTimestamp;

            // save submitted vote
            this._reactiveData.myVotes[this._reactiveData.poll.id] = this._reactiveData.selectedOptionID;
            LocalStorageWrapper.setItemAsync('myVotes', JSON.stringify(this._reactiveData.myVotes));
        }).bind(this));
    }).bind(_votingController)
});

Object.defineProperty(_votingController, 'submitVote', {
    value: (function (pollID, optionID, callback) {
        $$.ajax({
            url: 'http://akr-congress2018.ru/mobile-app/vote/' + pollID,
            //crossDomain: true,
            dataType: 'json',
            timeout: 5000,
            method: 'POST',
            data: {
                optionID: optionID,
                uniqueID: Math.random().toString() + (new Date()).getTime().toString()
            },
            success: function(data) {
                console.log(data);
                callback(data.poll, data.timestamp);
            },
            /*error: function (xhr, status) {
                console.log(xhr);
                console.log(status);
                if (status === 'timeout') {
                    callbackOnConnectivityFailure();
                } else {
                    callbackOnError(xhr.responseText);
                }
            }*/
        });
    }).bind(_votingController)
});



let VotingController = () => _votingController._construct();
export default VotingController;
