#!/usr/bin/env node

/**
 * Created by USER on 23.03.2017.
 * @see https://stackoverflow.com/a/33343120/2175025
 */

//
// This hook add 'Localizations' key
// into Info.plist for iOS
//

var fs = require('fs');
var plist = require('plist');

var FILE_PATH = 'platforms/ios/АКР 2018/АКР 2018-Info.plist';

module.exports = function(context) {
    if (!fs.existsSync(FILE_PATH))
        return;

    var xml = fs.readFileSync(FILE_PATH, 'utf8');
    var obj = plist.parse(xml);

    obj.CFBundleLocalizations = ['ru'];

    xml = plist.build(obj);
    fs.writeFileSync(FILE_PATH, xml, {encoding: 'utf8'});
};
