import Webpack from 'webpack';

export default {
    externals: {
        html10n: '_',
        framework7: 'Framework7',
        myApp: 'myApp',
        dom7: '$$',
        Dom7: 'Dom7',
    },
    node: {
        fs: 'empty'
    },
    entry: {
        poll: './www/templates/vue/poll/Main.js',
        main: './js/index.js',
    },
    output: {
        path: __dirname + '/www/js-bin',
        filename: '[name].js',
    },
    module: {
        loaders: [
            /*
             'style-loader' creates style nodes from JS strings
             'css-loader' translates CSS into CommonJS
             'less-loader' compiles Less to CSS
             */
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
            },
            {
                test: /\.vue$/,
                loader: 'vue-loader',
            },
            {
                test: /\.css$/,
                loaders: ['style-loader', 'css-loader'],
            },
            {
                test: /\.hbs/,
                loaders: ['null-loader'],
            },
            /*{
                test: /\.(png|jpg|gif|svg)$/,
                loaders: ['url-loader', 'file-loader'],
            },*/
        ],
    },
    resolve: {
        alias: {
            'vue$': 'vue/dist/vue.esm.js',
        },
    },
    //devtool: 'eval-source-map', // replace to 'source-map' on production
    devtool: 'source-map', // replace to 'eval-source-map' on development
    plugins: [
        new Webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery"
        }),
        /*new Webpack.DefinePlugin({
            'process.env': {
                'NODE_ENV': JSON.stringify('production')
            }
        }),*/
        new Webpack.optimize.UglifyJsPlugin({minimize: true}),
    ],
};
